
/**
 * @author Geanina Tambaliuc
 *
 */
public class Dangling {
	
	public static void main(String[] args) {
		
		int x=8;
//		/*Dangling else problem*/
//		if(x>0)
//			if(x<5)
//				System.out.println("X is bigger than 0 and smaller than 5");
//		else System.out.println("X is smaller than 0");
		
		
		/*Avoiding ambiguity (avoiding dangling else problem)*/
		if(x>0)
			{
				if(x<5)
				System.out.println("X is bigger than 0 and smaller than 5");
			}
		else System.out.println("X is smaller than 0");
	}
}
